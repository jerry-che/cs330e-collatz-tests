#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_eval_helper

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "999999 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999999)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "200 3000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  200)
        self.assertEqual(j, 3000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(9, 9)
        self.assertEqual(v, 20)

    def test_eval_6(self):
        v = collatz_eval(999999, 1)
        self.assertEqual(v, 525)

    def test_eval_7(self):
        v = collatz_eval(1, 10101)
        self.assertEqual(v, 262)

    # -----
    # eval_helper
    # -----

    def test_eval_helper_1(self):
        temp_dict = {}
        v = collatz_eval_helper(10, temp_dict)
        self.assertEqual(v, 7)
        self.assertEqual(
            temp_dict, {10: 7, 5: 6, 16: 5, 8: 4, 4: 3, 2: 2, 1: 1})

    def test_eval_helper_2(self):
        temp_dict = {10: 7, 5: 6, 16: 5, 8: 4, 4: 3, 2: 2, 1: 1}
        v = collatz_eval_helper(20, temp_dict)
        self.assertEqual(v, 8)
        self.assertEqual(
            temp_dict, {10: 7, 5: 6, 16: 5, 8: 4, 4: 3, 2: 2, 1: 1, 20: 8})

    def test_eval_helper_3(self):
        temp_dict = {}
        v = collatz_eval_helper(9, temp_dict)
        self.assertEqual(v, 20)
        self.assertNotEqual(temp_dict, {9: 20})

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 999999, 1, 525)
        self.assertEqual(w.getvalue(), "999999 1 525\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 10101, 262)
        self.assertEqual(w.getvalue(), "1 10101 262\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 999999\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_3(self):
        r = StringIO("9 9\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "9 9 20\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
...................
----------------------------------------------------------------------
Ran 19 tests in 12.700s

OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
...................
----------------------------------------------------------------------
Ran 19 tests in 12.700s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          36      0     14      0   100%
TestCollatz.py      84      0      0      0   100%
------------------------------------------------------------
TOTAL              120      0     14      0   100%

"""
